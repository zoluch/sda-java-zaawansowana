public class AccuntApplication {
    public static void main(String[] args) {
        Account account = new Account();
        Account account2 = new Account();
        account.setName("Konto premium");
        account.setBalance(500);
        account2.setName("Zwykłe konto");
        account2.setBalance(200);
        account.deposit(-3);
        account.deposit(200);
        account.withdraw(-5);
        account.withdraw(100);
        account.withdraw(2500);
        account.withdraw(1100);
        System.out.println("Nazwa: " + account.getName());
        System.out.println("Stan konta: " + account.getBalance());
        System.out.println("Transfer...");
        account.transfer(account2, 400);
        System.out.println(account.toString());
        System.out.println(account2.toString());
    }
}
