import java.util.Scanner;

public class DigitsInStringApp {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String usersText;
        do {
            System.out.println("Napisz tekst do sumowania cyfr z niego(\"exit\", żeby zakończyć).");
            usersText = scan.nextLine();
            DigitsInString text = new DigitsInString(usersText);
            System.out.println(text.sumDigits());
        } while(!usersText.equals("exit"));
    }
}
