public class Car {

    private int mileage = 0;
    private double fuelLevel = 0;
    private int tankCapacity;
    private double fuelConsumption;

    public Car(int tankCapacity, double fuelConsumption) {
        this.tankCapacity = tankCapacity;
        this.fuelConsumption = fuelConsumption;
    }

    public int getMileage() {
        return mileage;
    }

    public int drive(int distance) {
        int range = getCarRange();
        if(distance >= range) {
            mileage += range;
            fuelLevel = 0;
            return range;
        } else {
            mileage += distance;
            fuelLevel -= calculateFuelConsumption(distance);
            return distance;
        }
    }

    public double calculateFuelConsumption(int distance) {
        return fuelConsumption * distance / 100;
    }

    public double getFuelLevel() {
        return fuelLevel;
    }

    public double fillFuel() {
        double oldLevel = fuelLevel;
        fuelLevel = tankCapacity;
        return tankCapacity - oldLevel;
    }

    public double fillFuel(double fuelToFill) {
        double freeSpace = tankCapacity - fuelLevel;
        if(freeSpace < fuelToFill) {
            return fillFuel();
        } else {
            fuelLevel += fuelToFill;
            return fuelToFill;
        }
    }

    public int getCarRange() {
        int range = (int) (100 * (fuelLevel / fuelConsumption));
        return range;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mileage=" + mileage +
                ", fuelLevel=" + fuelLevel +
                ", tankCapacity=" + tankCapacity +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}
