import java.util.Arrays;
import java.util.Scanner;

public class ConvertingGallonsApp {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        final byte limit = 10;
        double[] gallons = new double[limit];
        double[] convertedToLiters = new double[limit];

        System.out.println("Wypisz 10 wartości od 1 do 100 w galonach");

        double checker = scan.nextDouble();
        for(int i = 0; i < limit; i++) {
            while(checker < 1 || checker > 100) {
                System.out.println("Podano liczbę spoza zakresu. Spróbuj ponownie.");
                checker = scan.nextDouble();
            }
            gallons[i] = checker;
            convertedToLiters[i] = checker * 3.7854;
            checker = scan.nextDouble();
        }

        System.out.println("Galony: " + Arrays.toString(gallons));
        System.out.println("Litry: " + Arrays.toString(convertedToLiters));
    }
}
