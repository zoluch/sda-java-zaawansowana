public class Text {

    public static String checkFirstChar(String text) {
        char firstSign = text.charAt(0);
        if(Character.isUpperCase(firstSign)) {
            return "Wielka litera";
        } else if(Character.isLowerCase(firstSign)) {
            return "Mała litera";
        } else if(Character.isDigit(firstSign)) {
            return "Cyfra";
        } else {
            return "Pomidor";
        }
    }
}
