import java.util.Arrays;

public class GallonConverter {

    //Zmienna przechowująca tablicę konwersji
    private final String[] convertedTable;

    public GallonConverter() {
        this.convertedTable = new String[100];
        fillTable();
    }

    //Wypełnia convertedTable wartościami
    private void fillTable() {
        for(int i = 0; i < 100; i++) {
            convertedTable[i] = (i+1) + " galonów to " + ((i+1)*3.7854) + " litrów.";
        }
    }

    //Zwraca convertedTable
    public String[] getGallonConverted() {
        return convertedTable;
    }

    @Override
    public String toString() {
        return "GallonConverter{" +
                "convertedTable=" + Arrays.toString(convertedTable) +
                '}';
    }

    public void printConvertedTable() {
        for(int i = 0; i < 100; i++) {
            if(i % 10 == 0) {
                System.out.println();
            }
            System.out.println(convertedTable[i]);
        }
    }
}
