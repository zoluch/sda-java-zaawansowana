package Printer;

public class ServiceApp {
    public static void main(String[] args) {

        String text = "MoJ TeeeksT";
        PrinterService pserv = new Service();

        PrInterface printer = new PrinterUpperCase();
        pserv.print(printer, text);

        printer = new PrinterLowerCase();
        pserv.print(printer, text);
    }
}
