package Printer;

public class PrinterApp {
    public static void main(String[] args) {

        String text = "mOj Supeer TEExt";

        PrInterface lowBlow = new PrinterLowerCase();
        lowBlow.printText(text);

        PrInterface upBlow = new PrinterUpperCase();
        upBlow.printText(text);
    }
}
