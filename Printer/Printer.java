package Printer;

public class Printer implements PrInterface {
    private String text;

    public Printer(String text) {
        this.text = text;
    }

    public void printText(String text) {
        System.out.println(text);
    }
}
