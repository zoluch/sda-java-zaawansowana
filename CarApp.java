public class CarApp {
    public static void main(String[] args) {

        Car car = new Car(60, 5);
        System.out.println(car.calculateFuelConsumption(22));
        System.out.println(car.fillFuel(50));
        System.out.println(car.drive(100));
        System.out.println(car.drive(2000));
        System.out.println(car);
    }
}
