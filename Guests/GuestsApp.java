package Guests;

import java.util.Scanner;

public class GuestsApp {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        IGuests guestLst = new Guests();

        while (true) {
            System.out.println("Podaj imię(\"exit\" - zakończ, \"lista\" - wyświetl aktualną listę gości).");
            String txt = scan.nextLine();
            if (txt.equals("exit")) {
                break;
            } else if (txt.equals("lista")) {
                System.out.println(guestLst.toString());
            } else if (!guestLst.addGuest(txt)) {
                System.out.println("Gość jest już na liście");
            }
        }
        System.out.println(guestLst.toString());
    }
}
