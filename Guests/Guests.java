package Guests;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Guests implements IGuests{
    private Set<String> guestList = new HashSet<>();

    @Override
    public boolean addGuest(String name) {
        return guestList.add(name);
    }

    @Override
    public List<String> getGuests() {
        List<String> listOfGuests = new ArrayList<>(guestList);
        return listOfGuests;
    }

    @Override
    public String toString() {
        return getGuests().toString();
    }
}
