package Guests;

import java.util.List;

public interface IGuests {
    boolean addGuest(String name);
    List<String> getGuests();
    String toString();
}
