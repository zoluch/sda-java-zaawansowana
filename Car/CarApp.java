package Car;

public class CarApp {
    public static void main(String[] args) {
        Manufacturer BMW = new Manufacturer("BMW", 1950, "Niemcy");
        Manufacturer Audi = new Manufacturer("Audi", 1947, "Niemcy");
        Manufacturer Toyota = new Manufacturer("Toyota", 1953, "Japonia");

        Car car1 = new Car("Nazwa", "Super", 20000, 2003, "V 12");
        Car car2 = new Car("Drugie auto", "Model", 5000, 1998, "V 8");
        Car car3 = new Car("Trzecie auto", "Super", 50000, 2003, "V 8");
        Car car4 = new Car("Nazwa", "Super", 20000, 2003, "V 12");

        car1.addManufacturer(BMW);
        car1.addManufacturer(Audi);
        car2.addManufacturer(BMW);
        car2.addManufacturer(Audi);
        car2.addManufacturer(Toyota);
        car3.addManufacturer(Toyota);
        car4.addManufacturer(BMW);
        car4.addManufacturer(Audi);

        CarService cars = new CarService();
        cars.addCar(car1);
        cars.addCar(car2);
        cars.addCar(car3);
        cars.addCar(car4);

        System.out.println(cars);
        System.out.println("Auta z silnikiem V 12" + cars.getCarsWithV12Engine());
        System.out.println("Auta wyprodukowane przed 1999" + cars.getBefore1999());
        System.out.println("Najdroższe auto: " + cars.getMostExpensiveCar());
        System.out.println("Najtańsze auto: " + cars.getCheapestCar());
        System.out.println("Auta z co najmniej 3 producentami: " + cars.getCarsWithMoreThan2Manufacturers());
        System.out.println("Czy auto znajduje się na liście ? " + cars.isOnTheList(car2));
        System.out.println("Auta wyprodukowane przez Toyotę: " + cars.carsFromManufacturer(Toyota));
        cars.removeCar(car4);
        System.out.println(cars);
    }
}
