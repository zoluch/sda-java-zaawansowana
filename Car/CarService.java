package Car;

import java.util.ArrayList;
import java.util.List;

public class CarService {

    private List<Car> cars = new ArrayList<>();

    public List<Car> getCars() {
        return cars;
    }

    public void addCar(Car car) {
        this.cars.add(car);
    }

    public void removeCar(Car car) {
        cars.removeIf(car::equals);
    }

    public List<Car> getCarsWithV12Engine() {
        List<Car> carsWithV12 = new ArrayList<>();
        for (Car car : cars) {
            if (car.getEngineType().equals("V 12")) {
                carsWithV12.add(car);
            }
        }
        return carsWithV12;
    }

    public List<Car> getBefore1999() {
        List<Car> carsBefore1999 = new ArrayList<>();
        for (Car car : cars) {
            if (car.getYear() < 1999) {
                carsBefore1999.add(car);
            }
        }
        return carsBefore1999;
    }

    public Car getMostExpensiveCar() {
        Car mostExpensive = cars.get(0);
        for (int i = 1; i < cars.size(); i++) {
            if (cars.get(i).getPrice() > mostExpensive.getPrice()) {
                mostExpensive = cars.get(i);
            }
        }
        return mostExpensive;
    }

    public Car getCheapestCar() {
        Car cheapest = cars.get(0);
        for (int i = 1; i < cars.size(); i++) {
            if (cars.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = cars.get(i);
            }
        }
        return cheapest;
    }

    public List<Car> getCarsWithMoreThan2Manufacturers() {
        List<Car> carsWithManyManufacturers = new ArrayList<>();
        for (Car car : cars) {
            if (car.getManufacturers().size() > 2) {
                carsWithManyManufacturers.add(car);
            }
        }
        return carsWithManyManufacturers;
    }

    public boolean isOnTheList(Car car) {
        boolean isTrue = false;
        for (int i = 0; i < cars.size(); i++) {
            if (car.equals(cars.get(i))) {
                isTrue = true;
                break;
            }
        }
        return isTrue;
    }

    public List<Car> carsFromManufacturer(Manufacturer manufacturer) {
        List<Car> carsFromManufacturer = new ArrayList<>();
        for (Car car : cars) {
            for (int i = 0; i < car.getManufacturers().size(); i++) {
                if (car.getManufacturers().get(i).equals(manufacturer)) {
                    carsFromManufacturer.add(car);
                    break;
                }
            }
        }
        return carsFromManufacturer;
    }

    @Override
    public String toString() {
        return "CarService{" +
                "cars=" + cars +
                '}';
    }
}
