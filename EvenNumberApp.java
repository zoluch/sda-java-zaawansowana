import java.util.Scanner;

public class EvenNumberApp {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą");
        EvenNumber number = new EvenNumber(scan.nextInt());
        scan.nextLine();
        if(number.isEven()) {
            System.out.println("Liczba jest parzysta");
        } else {
            System.out.println("Liczba jest nieparzysta");
        }
    }
}
