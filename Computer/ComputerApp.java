package Computer;

public class ComputerApp {

    public static void main(String[] args) {

        Computer computer1 = new Computer("Procesor", "8gb", "Karta graficzna", "Lenovo", "Model");
        System.out.println(computer1.toString());

        Computer computer2 = new Computer("Procesor", "8gb", "Karta graficzna", "Lenovo", "Model");
        Computer computer3 = new Computer("Procesor", "2gb", "Karta graficzna", "Lenovo", "Model");
        System.out.println(computer1.equals(computer2));
        System.out.println(computer2.equals(computer3));

        Laptop laptop1 = new Laptop("Procesor", "8gb", "Karta graficzna", "Lenovo", "Model", "Bateria");
        System.out.println(laptop1);
        Laptop laptop2 = new Laptop("Procesor", "8gb", "Karta graficzna", "Lenovo", "Model", "Bateria");
        laptop2.setBrand("Marka");
        System.out.println(laptop1.equals(laptop2));
        System.out.println(computer1.hashCode());
        System.out.println(laptop2.hashCode());
        System.out.println(laptop2);
    }
}
