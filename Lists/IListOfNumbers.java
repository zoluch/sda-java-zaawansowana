package Lists;

public interface IListOfNumbers {
    double sum();

    double average();

    double biggest();

    double smallest();

    void addNumber(Double number);
}
