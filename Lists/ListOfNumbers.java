package Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfNumbers implements IListOfNumbers {
    private List<Double> numbers = new ArrayList<Double>();

    public void addNumber(Double number) {
        numbers.add(number);
    }

    @Override
    public double sum() {
        double sum = 0;
        for (Double number : numbers) {
            sum += number;
        }
        return sum;
    }

    @Override
    public double average() {
        return sum() / numbers.size();
    }

    @Override
    public double biggest() {
        double max = numbers.get(0);
        for (int i = 1; i < numbers.size(); i++) {
            if (max < numbers.get(i)) {
                max = numbers.get(i);
            }
        }
        return max;
    }

    @Override
    public double smallest() {
        return Collections.min(numbers);
    }
}
