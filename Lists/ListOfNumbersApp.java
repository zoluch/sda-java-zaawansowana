package Lists;

import java.util.List;
import java.util.Scanner;

public class ListOfNumbersApp {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        IListOfNumbers numbers = new ListOfNumbers();
        System.out.println("Wypisz liczby(\"end\", żeby zakończyć)");
        do {
            String x = scan.next();
            if (x.equals("end")) {
                break;
            }
            numbers.addNumber(Double.parseDouble(x));
        } while (true);

        System.out.println("Suma liczb: " + numbers.sum());
        System.out.println("Średnia arytmetyczna: " + numbers.average());
        System.out.println("Największa podana liczba: " + numbers.biggest());
        System.out.println("Najmniejsza podana liczba: " + numbers.smallest());
    }
}
