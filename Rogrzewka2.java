public class Rogrzewka2 {

    public static void main(String[] args) {

        String word1 = "text";
        String word2 = "text";
        String word3 = "t" + "ext";
        String word4 = new String("text");

        System.out.println(word1 == word2);
        System.out.println(word2 == word3);
        System.out.println(word1 == word4);

        System.out.println(word1.equals(word4));
    }
}
