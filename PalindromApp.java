import java.util.Scanner;

public class PalindromApp {
    public static void main(String[] args) {
        System.out.println("Wpisz słowo do sprawdzenia");
        Scanner scan = new Scanner(System.in);
        Palindrome usersText = new Palindrome(scan.nextLine());
        if(usersText.isPalindrome()) {
            System.out.println("Tak, jest palindromem");
        } else {
            System.out.println("Nie jest palindromem");
        }
    }
}
