public class Account {
    private String name;
    private int balance = 0;
    private boolean debit = false;
    private final int debitLimit = -1000;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void deposit(int amount) {
        if (amount>0){
            System.out.print("Stan konta: " + balance + "| Wpłata: " + amount + "|");
            balance += amount;
            System.out.print("Po wpłacie: " + balance + "\n");
            if(balance+amount > 0) {
                debit=false;
            }
        } else {
            System.out.println("Wartość musi być dodatnia!");
        }
    }

    public void withdraw(int amount) {
        if (amount > 0 && balance-amount > debitLimit) {
            System.out.print("Stan konta: " + balance + "| Wypłata: " + amount + "|");
            balance -= amount;
            System.out.print("Po wypłacie: " + balance + "\n");
        } else if(amount <= 0){
            System.out.println("Wartość musi być dodatnia");
        } else {
            System.out.println("Nie można wykonać operacji przekraczającej debet.");
        }
        if (balance < 0) {
            debit = true;
        }
        if(debit) {
            System.out.println("Ujemny stan konta");
        }
    }

    public void transfer(Account other, int amount) {
        if (amount<= 0) {
            System.out.println("Kwota transferu musi być dodatnia!");
            return;
        }

        if(this.balance - amount >debitLimit) {
            this.withdraw(amount);
            other.deposit(amount);
        } else {
            System.out.println("Brak wystarczających środków.");
        }
    }

    public String toString() {
        return "Account{name: " + name + ", balance:" + balance + "}";
    }
}
