import java.util.Arrays;
public class ListaGosci {
    private String[] listaGosci;
    private int counter;
    public ListaGosci(int liczbaGosci) {
        this.listaGosci = new String[liczbaGosci];
    }
    public void dodajGoscia(String imie) {
        if (counter >= listaGosci.length) {
            System.out.println("Lista jest pelna");
            return;
        }
        for (int i = 0; i < listaGosci.length; i++) {
            if (imie.equals(listaGosci[i])) {
                System.out.println("Gosc jest juz na liscie");
                return;
            }
        }
        listaGosci[counter++] = imie;
//        counter++;
    }
    public String getlistaGosci() {
        return Arrays.toString(listaGosci);
    }
    public boolean isListaGosciPelna (){
        return counter >= listaGosci.length;
    }
}