import java.util.InputMismatchException;
import java.util.Scanner;

public class Wyjatek {

    public static void main(String[] args) {

        int[] intTable = new int[5];
        System.out.println("Podaj liczby:");
        Scanner scan = new Scanner(System.in);
        int number;
        for(int i = 0; i < intTable.length; i++) {
                try {
                    number = scan.nextInt();
                    intTable[i] = number;
                } catch (InputMismatchException e) {
                    System.out.println("Błąd");
                    scan.nextLine();
                    i--;
                }
        }
        for(int i = 0; i < intTable.length; i++) {
            System.out.println(intTable[i]);
        }

    }
}
