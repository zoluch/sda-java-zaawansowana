package Person;

import java.util.ArrayList;
import java.util.List;


public class PersonApp {
    public static void main(String[] args) {

        Person person1 = new Person("Przemek", "Dy", "9");
        Person person2 = new Person("Przemcio", "Dy", "9");
        Person person3 = new Person("Przemek", "Dy", "9");
        Person person4 = new Person("Przemek", "Dy", "11");
        Person person5 = new Person("Przemcio", "Dy", "9");
        List<Person> listOfPeople = new ArrayList<>();
        listOfPeople.add(person1);
        listOfPeople.add(person2);
        listOfPeople.add(person3);
        listOfPeople.add(person4);
        listOfPeople.add(person5);
        listOfPeople.remove(person3);
        System.out.println(listOfPeople);
    }
}
