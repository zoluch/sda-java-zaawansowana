class Palindrome {
    private String text;

    public Palindrome(String text) {
        this.text = text;
    }

    public boolean isPalindrome() {
        int j = text.length()-1;
        boolean isTrue = true;
        for(int i = 0; i < (text.length())/2; i++) {
            if(text.charAt(i) != text.charAt(j)) {
                isTrue = false;
                break;
            }
            j--;
        }
        return isTrue;
    }
}
