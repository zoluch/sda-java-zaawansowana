public class DigitsInString {
    private String text;

    public DigitsInString(String text) {
        this.text = text;
    }

    private String getDigitsFromText() {
        String newText = text.replaceAll("[^\\d]", "");
        return newText;
    }

    public int sumDigits() {
        String digitsInText = getDigitsFromText();
        int sum = 0;
        for(int i = 0; i < digitsInText.length(); i++) {
            sum += Character.getNumericValue(digitsInText.charAt(i));
        }
        return sum;
    }
}
