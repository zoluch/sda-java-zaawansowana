public class Rozgrzewka3 {

    public static void main(String[] args) {

        Integer number1 = 100;
        Integer number2 = 100;
        System.out.println(number1 == number2);

        Integer number3 = 200;
        Integer number4 = 200;
        System.out.println(number3 == number4);
        //java cache w przedziale -128 do 128

        System.out.println(number1.equals(number2));
        System.out.println(number3.equals(number4));

        int number5 = 200;
        int number6 = 200;
        System.out.println(number5 == number6);
    }
}
